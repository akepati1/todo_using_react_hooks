import React from "react";
import Element from "./Element";
import "../todo.css";
import spinner from "../loading-icon-animated-gif-19.gif"
import { useMemo } from "react";
import { useSelector, useDispatch } from "react-redux";
import * as actions from "../redux/actions";

function Todo() {
  const items = useSelector((state) => state.items);
  const userInput = useSelector((state) => state.userInput);
  const filterType = useSelector((state) => state.filterType);
  const editId = useSelector((state) => state.editId);
  let isLodder = useSelector((state) => state.isLodder);
  
  const dispatch = useDispatch();

  const handleAdd = () => {
    let obj = {
      id: Math.round(Math.random() * 100), // UUID
      name: userInput,
      isCompleted: false,
    };

    dispatch(actions.asyncAdd(obj));
  };
  let leftItems = useMemo(
    () =>
      items.filter((item) => {
        return item.isCompleted === false;
      }).length,
    [items]
  );

  let displayArray = useMemo(() => {
    if (filterType === "Active") {
      return items.filter((item) => item.isCompleted === false);
    } else if (filterType === "Completed") {
      return items.filter((item) => item.isCompleted === true);
    } else {
      return items;
    }
  }, [items, filterType]);
  let element = isLodder===true  ?{spinner} :<h2>not loding</h2>
  
  return (
   
    <div className="container-todo">
      <p>TODO-LIST</p>
      <button onClick={() => dispatch(actions.reset())}>Reset</button>
      <div className="todo">
        <div className="input-add">
          <input
            className="input"
            type="text"
            value={userInput}
            onChange={(e) => dispatch(actions.userInput(e.target.value))}
            placeholder="What needs to done?"
          ></input>
          <button onClick={handleAdd}>Add Task</button>
        </div>
        {isLodder===true ?<div><img className="spinner"src={spinner}></img></div>: displayArray.map((data) => {
          return (
            <Element
              key={data.id}
              data={data}
              editValue={editId}
              onEdit={(setUpdateValue, data, updateValue) =>
                dispatch(
                  actions.handleEditTodo(setUpdateValue, data, updateValue)
                )
              }
              onDelete={(id) => dispatch(actions.deleteTodo(id))}
              toggle={(data) => dispatch(actions.handleCompleteTodo(data))}
            />
          );
        })}
        <div className="options">
         
          <div className="items-left">
            <span>{leftItems}</span> items left
          </div>
          <div className="filters">
            <button
              className={filterType === "All" ? "border-filter" : ""}
              onClick={() => dispatch(actions.setFilterType("All"))}
            >
              All
            </button>
            <button
              className={filterType === "Active" ? "border-filter" : ""}
              onClick={() => dispatch(actions.setFilterType("Active"))}
            >
              Active
            </button>
            <button
              className={filterType === "Completed" ? "border-filter" : ""}
              onClick={() => dispatch(actions.setFilterType("Completed"))}
            >
              Completed
            </button>
          </div>
          <div onClick={() => dispatch(actions.handleClearTodo())}>
            Clear-completed
          </div>
        </div>
      </div>
      <div className=" dummydiv"></div>
      <div className=" dummydiv"></div>
    </div>
  );
}
export default Todo;
