export function addTodo (item){
     return {
        type:"addTodo",
        payload:item
     }
}
export function deleteTodo (deleteId){
    return {
       type:"deleteTodo",
       payload:{
        deleteId
       }
    }
}
export function isLoder (){
    return {
        type:"isLoaderToTrue"
    }
}
export function added (){
    return {
        type:"isLoaderTofalse"
    }
}
export function userInput(value){
    return {
        type:"userInput",
        payload:value
    }
}
export function reset(){
    return {
        type:"resetTodo",
        payload:{}
    }
}
export function handleEditTodo (data){
    return {
       type:"handleEditTodo",
       payload:{
        
        data
        
       }
    }
}
export function updateTodo(todo){
    return{
        type:"updateTodo",
        payload:{
            value:todo
        }
    }
}
export function setFilterType(value){
    return {
        type:"setFilterType",
        payload:value
    }
}
export function saveTodo (data,updateValue){
    return {
       type:"saveTodo",
       payload:{
        data,
        updateValue
       }
    }
}
export function handleCompleteTodo (data){
    return {
       type:"handleCompleteTodo",
       payload:{
        data,
       
       }
    }
}
export function handleClearTodo (){
    return {
       type:"handleClearTodo",
       
    }
}
/* middle-wares */ 
export function asyncAdd(item){
    return (dispatch)=>{
        dispatch(isLoder())
        setTimeout(()=>{
            dispatch(addTodo(item))
            dispatch(added())
        },300)
      
    }
}